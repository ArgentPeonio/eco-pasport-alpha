package main

import (
	"eco-passport-21/config"
	"eco-passport-21/db"
	"eco-passport-21/web"
	"flag"
	"fmt"

	"gitlab.com/gbh007/gojlog"
	"gitlab.com/krasecology/go-lib/db/model"
)

func main() {
	gojlog.AddTrimPathAuto()
	configPath := flag.String("c", "config.json", "фаил конфигурации")
	staticDir := flag.String("s", "static", "папка с файлами для раздачи веб сервера")
	webPort := flag.Int("p", 80, "порт веб-сервера")
	flag.Parse()

	if config.Load(*configPath) != nil {
		gojlog.Critical("Ошибка загрузки конфигурации")
		return
	}
	cnf := config.GetConfig()
	// Подключение к базе авторизации
	if err := model.Connect(
		cnf.Mongo.URI,
		cnf.Mongo.DBName,
		cnf.Mongo.User,
		cnf.Mongo.Password,
	); err != nil {
		gojlog.Critical("ошибка соединения с базой библиотеки")
		return
	}

	if err := db.Connect(); err != nil {
		gojlog.Error(err)
		return
	}
	done := web.Run(fmt.Sprintf(":%d", *webPort), *staticDir)
	gojlog.Warning("Сервер запущен")
	<-done
	gojlog.Warning("Сервер остановлен")
}
