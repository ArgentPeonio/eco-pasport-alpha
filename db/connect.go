package db

import (
	"database/sql"
	"eco-passport-21/config"
	"encoding/json"
	"fmt"

	//Надо
	_ "github.com/lib/pq"
)

var _db *sql.DB

type nullString struct {
	sql.NullString
}

func (ns *nullString) UnmarshalJSON(b []byte) error {
	tmp := nullString{}
	if err := json.Unmarshal(b, &tmp.String); err != nil {
		return err
	}
	if tmp.String != "" {
		tmp.Valid = true
	}
	*ns = tmp
	return nil
}

func (ns nullString) MarshalJSON() ([]byte, error) {
	return json.Marshal(ns.String)
}

// GetDatabase получение базы
func GetDatabase() *sql.DB {
	return _db
}

// Connect подключение к базе
func Connect() (err error) {
	cnf := config.GetConfig()
	cs := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%d sslmode=disable",
		cnf.Postgres.User,
		cnf.Postgres.Password,
		cnf.Postgres.DBName,
		cnf.Postgres.URI,
		cnf.Postgres.Port)
	_db, err = sql.Open("postgres", cs)
	if err != nil {
		return err
	}
	return _db.Ping()
}
