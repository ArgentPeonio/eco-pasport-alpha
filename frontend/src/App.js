import React, { Component } from 'react';
import { Load } from './base/load';
import { Treebeard } from 'react-treebeard';
import { treeStyle } from './treeStyle';
import "./App.css";
import { useTable } from 'react-table';

function Table({ columns, data }){
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable({
        columns,
        data,
    })

    return (
        <table {...getTableProps()}>
            <thead>
                {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map(column => (
                            <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                        ))}
                    </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {rows.map((row, i) => {
                    prepareRow(row)
                    return (
                        <tr {...row.getRowProps()}>
                            {row.cells.map(cell => {
                                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                            })}
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            years: [],
            year: 2020,
            regions: {},
            tree: {},
            selectedRegionID: 0,
            selectedTableID: 0,
            districts: [],
            towns: [],
            header: [],
            table: [],
        };
        this.onToggle = this.onToggle.bind(this);
    }

    
    componentDidMount() {
        new Load(this.context, "Получение регионов", "/api/table/get-region")
            .SetErr()
            .Load()
            .then((regions) => {
                this.setState({
                    regions: regions
                });
            });
        new Load(this.context, "Получение годов", "/api/table/get-year")
            .SetErr()
            .Load()
            .then((years) => {
                this.setState({
                    years: years
                });
            });
        //this.interval = setInterval(() => this.setState(this.regSplit), 1000);
        
    }
    componentDidUpdate(_, prevState) {
        if (this.state.selectedRegionID !== prevState.selectedRegionID ||
            this.state.year !== prevState.year) {
            this.getTree();
        }
        if (this.state.regions !== prevState.regions) {
            this.regSplit();
        }
        if (this.state.towns !== prevState.towns){
            this.regButton(this.state.towns);
        }
        if (this.state.districts !== prevState.districts){
            this.regButton(this.state.districts);
        }
        if (this.state.selectedTableID !== prevState.selectedTableID){
            this.getHeader();
            this.getTable();
        }
    }
    /*componentWillUnmount(){
        clearInterval(timer)
    }*/
    getTree() {
        new Load(this.context, "Получение дерева", "/api/table/get-tree", { year: this.state.year })
            .SetErr()
            .Load()
            .then((tree) => {
                this.setState({
                    tree: tree
                });
            });
    }
    getHeader() {
        new Load(this.context, "Получение заголовков", "/api/table/get-header", { tableId: this.state.selectedTableID })
            .SetErr()
            .Load()
            .then((header) => {
                this.setState({
                    header: header
                });
            });
    }
    getTable() {
        new Load(this.context, "Получение таблицы", "/api/table/get-table", { headerId: this.state.selectedTableID, regionId: this.state.selectedRegionID, year: this.state.year })
            .SetErr()
            .Load()
            .then((table) => {
                this.setState({
                    table: table
                });
            });
    }

    onToggle(node, toggled) {
        const { cursor, data } = this.state.tree;
        if (cursor) {
            this.setState(() => ({ cursor, active: false }));
        }
        node.active = true;
        if (node.children) {
            node.toggled = toggled;
        }
        (node.id > 0) ? this.setState({ selectedTableID: node.id }) :
            this.setState(() => ({ cursor: node, data: Object.assign({}, data) }));


    }

    
    regSplit() {
        /*const timer = setInterval(() => {
            this.state.towns.push(this.state.regions['town'])
            this.state.districts.push(this.state.regions['district'])
        }, 1000);
        return () => clearInterval(timer);*/
        this.state.towns.push(this.state.regions['town'])
        this.state.districts.push(this.state.regions['district'])
    }

    regButton(array){
        console.log(array)
        return array.map((key) => 
            Object.keys(key).map(place => (<button
                key={key[place].id}
                onClick={() => this.setState({
                    selectedRegionID: key[place].id
                })}>{key[place].name}</button>)))
    }

    render() {
        
        const header = this.state.header
        const tree = this.state.tree
        //const regionList = this.state.regions
        const years = this.state.years
        const towns = this.state.towns
        const districts = this.state.districts

        return (

            <div>
                <div className='years'>
                    {years.map(y => (<button key={y.id}
                        onClick={() => this.setState({ year: y })}>{y.toString()}</button>))}
                </div>
                <div className='regions'>
                    <div className='column'>
                        {//попробовать вынести функцию в кнопку. пока оно рисует кнопку до забивания данных в дистриктс
                            this.regButton(districts)}
                    </div>
                    <div className='column'>
                        {this.regButton(towns)}
                    </div>
                </div>

                <br />
                <div>
                    <Treebeard
                        data={tree}
                        onToggle={this.onToggle}
                        style={treeStyle}
                    /></div>
            </div>
        );
    }
}

export default App;
