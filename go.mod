module eco-passport-21

go 1.16

require (
	github.com/lib/pq v1.10.4
	gitlab.com/gbh007/gojlog v1.3.1
	gitlab.com/krasecology/go-lib v0.0.15
)
