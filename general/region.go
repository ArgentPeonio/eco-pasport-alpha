package general

import (
	"eco-passport-21/db"

	"gitlab.com/gbh007/gojlog"
)

//Regions структура для передачи данных о регионах, разделенных на города и округа
type Regions struct {
	District []Region `json:"district"`
	Town     []Region `json:"town"`
}

//Region структура, представляющая информацию об одном конкретном регионе
type Region struct {
	ID   int     `json:"id"`
	Name string  `json:"name"`
	Lat  float64 `json:"lat"`
	Lng  float64 `json:"lng"`
}

//RegionReq запрашивает список регионов и разделяет их на округа и города
func RegionReq() (regions Regions) {
	type RawRegions struct {
		ID     int
		Name   string
		Lat    float64
		Lng    float64
		Istown bool
	}
	_db := db.GetDatabase()
	regionsReq := `select
						id,
						"name",
						lat,
						lng,
						istown
				   from
						"eco-pasport".eco_pasport."Region" r
				   order by
						sort_region`

	reg, err := _db.Query(regionsReq)
	if err != nil {
		gojlog.Error(err)
		return
	}
	var region Region
	for reg.Next() {
		i := RawRegions{}
		err := reg.Scan(&i.ID, &i.Name, &i.Lat, &i.Lng, &i.Istown)
		if err != nil {
			gojlog.Error(err)
			return
		}
		if !i.Istown {
			region.ID = i.ID
			region.Name = i.Name
			region.Lat = i.Lat
			region.Lng = i.Lng
			regions.District = append(regions.District, region)
		} else {
			region.ID = i.ID
			region.Name = i.Name
			region.Lat = i.Lat
			region.Lng = i.Lng
			regions.Town = append(regions.Town, region)
		}
	}
	return
}
