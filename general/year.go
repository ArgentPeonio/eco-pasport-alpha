package general

import (
	"eco-passport-21/db"

	"gitlab.com/gbh007/gojlog"
)

//YearReq запрашивает все встречающиеся в базе года для отображения в селекторе года и дальнейшей фильтрации информации
func YearReq() (years []int) {
	_db := db.GetDatabase()
	yearsRequest := `select
						t."year"
					from
						"eco-pasport".eco_pasport."Table" t
					group by
						t."year"`

	y, err := _db.Query(yearsRequest)
	if err != nil {
		gojlog.Error(err)
		return nil
	}

	var year int
	for y.Next() {
		err := y.Scan(&year)
		if err != nil {
			gojlog.Error(err)
			return nil
		}
		years = append(years, year)
	}
	return
}
