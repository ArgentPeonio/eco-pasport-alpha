package table

import (
	"eco-passport-21/db"
	"encoding/json"

	"gitlab.com/gbh007/gojlog"
)

//Header служит для записи в себя заголовка с базы
type Header struct {
	Text     string   `json:"text"`
	Sub      []Header `json:"sub"`
}

//FrontHeader формат структуры для передачи на фронт
type FrontHeader struct {
	Header string `json:"header"`
	Accessor int `json:"accessor"`
	Columns []FrontHeader `json:"columns"`
}
//HeaderReq запрашивает заголовки таблиц
func HeaderReq(tableID int) (frontHeader []FrontHeader) {
	_db := db.GetDatabase()
	headerReq := `select
					header
				  from
					"eco-pasport".eco_pasport."Header" h
				  where
					id = $1`

	var headerRaw []byte
	var headJSON []Header
	req, err := _db.Query(headerReq, tableID)
	if err != nil {
		gojlog.Error(err)
	}
	for req.Next() {
		if err := req.Scan(&headerRaw); err != nil {
			gojlog.Error(err)
		}
		if err = json.Unmarshal(headerRaw, &headJSON); err != nil {
			gojlog.Error(err)
		}

		i := 1
		var frontHeaderEl FrontHeader
		for j, head := range headJSON {
			frontHeader = append(frontHeader, frontHeaderEl)
			frontHeader[j].Header = head.Text
			if len(head.Sub) == 0 {
				frontHeader[j].Accessor = i
				i++
			} else {
				for k, head2 := range head.Sub {
					frontHeader[j].Columns = append(frontHeader[j].Columns, frontHeaderEl)
					frontHeader[j].Columns[k].Header = head2.Text
					if len(head2.Sub) == 0 {
						frontHeader[j].Columns[k].Accessor = i
						i++
					} else {
						for l, head3 := range head2.Sub {
							frontHeader[j].Columns[k].Columns = append(frontHeader[j].Columns[k].Columns, frontHeaderEl)
							frontHeader[j].Columns[k].Columns[l].Header = head3.Text
							if len(head3.Sub) == 0 {
								frontHeader[j].Columns[k].Columns[l].Accessor = i
								i++
							}
						}
					}
				}
			}
		}
		i = 0
	}
	return
}
