package table

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/gbh007/gojlog"
)

//JSONTree структура, описывающая дерево в общем
/*type JSONTree struct {
	Lable string        `json:"label"`
	Children []JSONSection `json:"child"`
}

//JSONSection структура, описывающая разделы в дереве
type JSONSection struct {
	Right []string    `json:"right"`
	Name string      `json:"section_label"`
	Children []JSONTable `json:"child"`
}

//JSONTable структура, описывающая отдельные таблицы дерева
type JSONTable struct {
	ID    int      `json:"id"`
	Right []string `json:"right"`
	Lable string   `json:"table_label"`
}*/

//JSONTree2 универсальная структура, описывающая один элемент дерева, не зависимо от уровня вложенности
type JSONTree2 struct {
	Name     string      `json:"name"`
	Right    []string    `json:"right"`
	ID       int         `json:"id"`
	Children []JSONTree2 `json:"children"`
}

var _jsontree JSONTree2

//TreeSort получает информацию о пользователе и год, на основе этого отдает дерево с доступными таблицами
func TreeSort(UserPerms []string, year int) (tree JSONTree2) {
	//year := 2021
	treePath := fmt.Sprintf(".hidden/%dtree.json", year)
	file, err := os.Open(treePath)
	if err != nil {
		gojlog.Error(err)
		return
	}
	defer file.Close()
	d := json.NewDecoder(file)
	if err := d.Decode(&_jsontree); err != nil {
		gojlog.Error(err)
		return
	}

	// ! Name'ы таблиц можно брать с базы мини-запросом по айди таблицы
	// ! Количество прав у раздела (section) всегда должно быть меньше или равно количеству прав у таблиц (table). Разные таблицы в одном разделе могут иметь разные наборы прав
	// ! раздел должен включать наименьшее общее.
	//Расширяем массив кальк под размер ясонки, иначе не получится записывать в него информацию.
	calc := treeCalc(_jsontree)
	//Собираем дерево с проверкой прав на отображение разделов и таблиц
	//Заголовок. Содержание по вкусу.
	calc.Name = _jsontree.Name

	//Берем весь объем прав у пользователя
	if len(UserPerms) > 0 {
		for _, permis := range UserPerms {
			//Перебираем разделы (section) в ясонке
			for i, section := range _jsontree.Children {

				//Если у раздела нет прав для просмотра, т.е. он предполагается общедоступным (раздел уровня 1)
				if len(section.Right) == 0 {
					calc.Children[i].Name = section.Name
					//Раздел уровня 1.2
					for j, section2 := range section.Children {
						calc.Children[i].Children[j].Name = section2.Name
						//Раздел уровня 1.2.3
						for k, section3 := range section2.Children {
							//если больше 0 то это раздел но его чаилды таблицы
							if len(section3.Children) > 0 {
								calc.Children[i].Children[j].Children[k].Name = section3.Name
								for l, table := range section3.Children {
									//Если у таблицы нет ограничений к просмотру
									if len(table.Right) == 0 {
										calc.Children[i].Children[j].Children[k].Children[l].ID = table.ID
										calc.Children[i].Children[j].Children[k].Children[l].Name = table.Name
									} else { //Если ограничения все же есть, перебираем их, чтобы понять, может ли пользователь видеть таблицу
										for _, tablePerm := range table.Right {
											if tablePerm == permis {
												calc.Children[i].Children[j].Children[k].Children[l].ID = table.ID
												calc.Children[i].Children[j].Children[k].Children[l].Name = table.Name
											}
										}
									}

								} //если не больше ноля то это уже таблица уровня 2.1
							} else {
								//Если у таблицы нет ограничений к просмотру
								if len(section3.Right) == 0 {
									calc.Children[i].Children[j].Children[k].ID = section3.ID
									calc.Children[i].Children[j].Children[k].Name = section3.Name
								} else { //Если ограничения все же есть, перебираем их, чтобы понять, может ли пользователь видеть таблицу
									for _, tablePerm := range section3.Right {
										if tablePerm == permis {
											calc.Children[i].Children[j].Children[k].ID = section3.ID
											calc.Children[i].Children[j].Children[k].Name = section3.Name
										}
									}
								}

							}
						}
						/*//Общедоступный раздел, не значит что все таблицы в нем будут так же доступны. Перебираем таблицы в разделе
						for j, table := range section.Children {
							//Если у таблицы нет ограничений к просмотру
							if len(table.Right) == 0 {
								calc.Children[i].Children[j].Children[k].ID = table.ID
								calc.Children[i].Children[j].Children[k].Name = table.Name
							} else { //Если ограничения все же есть, перебираем их, чтобы понять, может ли пользователь видеть таблицу
								for _, tablePerm := range table.Right {
									if tablePerm == permis {
										calc.Children[i].Children[j].Children[k].ID = table.ID
										calc.Children[i].Children[j].Children[k].Name = table.Name
									}
								}
							}

						}*/
					}
				} /*else { //Если весь раздел имеет какие-либо ограничения к просмотру
					for _, sectionPerm := range section.Right {
						if sectionPerm == permis {
							calc.Children[i].Name = section.Name
							for k, section2 := range section.Children  {
								for j, table := range section2.Children {
									//Проверяем, какие таблицы в разделе может видеть юзер
									for _, tablePerm := range table.Right {
										if tablePerm == permis {
											calc.Children[i].Children[j].ID = table.ID
											calc.Children[i].Children[j].Name = table.Name
										}
									}
								}
							}
						}
					}
				}*/
			}
		}
	} else { //Если у юзера нет каких-либо прав (просто зарегестрирован), либо если он не авторизован. Отдает только общедоступные таблицы
		for i, section := range _jsontree.Children {
			//Если у раздела нет прав для просмотра, т.е. он предполагается общедоступным (раздел уровня 1)
			if len(section.Right) == 0 {
				calc.Children[i].Name = section.Name
				//Раздел уровня 1.2
				for j, section2 := range section.Children {
					calc.Children[i].Children[j].Name = section2.Name
					//Раздел уровня 1.2.3
					for k, section3 := range section2.Children {
						//если больше 0 то это раздел но его чаилды таблицы
						if len(section3.Children) > 0 {
							calc.Children[i].Children[j].Children[k].Name = section3.Name
							for l, table := range section3.Children {
								//Если у таблицы нет ограничений к просмотру
								if len(table.Right) == 0 {
									calc.Children[i].Children[j].Children[k].Children[l].ID = table.ID
									calc.Children[i].Children[j].Children[k].Children[l].Name = table.Name

								}

							} //если не больше ноля то это уже таблица уровня 2.1
						} else {
							//Если у таблицы нет ограничений к просмотру
							if len(section3.Right) == 0 {
								calc.Children[i].Children[j].Children[k].ID = section3.ID
								calc.Children[i].Children[j].Children[k].Name = section3.Name
							}

						}
					}
				}
			}
		}
	}

	//Отсекаем лишнее - пустые разделы и таблицы
	for i, section := range calc.Children {
		for j, section2 := range section.Children {
			for k, section3 := range section2.Children {
				for l, section4 := range section3.Children {
					if len(section4.Name) == 0 && section4.ID == 0 {
						calc.Children[i].Children[j].Children[k].Children = append(calc.Children[i].Children[j].Children[k].Children[:l], calc.Children[i].Children[j].Children[k].Children[l+1:]...)
						if len(section4.Name) == 0 && section3.ID == 0 {
							//fmt.Println(len(section3.Children))
							calc.Children[i].Children[j].Children = append(calc.Children[i].Children[j].Children[:k], calc.Children[i].Children[j].Children[k+1:]...)
							if len(section3.Name) == 0 && section2.ID == 0 {
								calc.Children[i].Children = append(calc.Children[i].Children[:j], calc.Children[i].Children[j+1:]...)
								if len(section2.Name) == 0 && section.ID == 0 {
									calc.Children = append(calc.Children[:i], calc.Children[i+1:]...)
								}
							}
						}
					}
				}
			}
		}
	}

	//Записываем инфу в конечный массив для отдачи фронту
	tree = calc
	return
}

//Расширяем массив calc под размер ясонки
func treeCalc(file JSONTree2) (calculated JSONTree2) {
	treeSection := JSONTree2{}
	for i, section := range file.Children {
		if len(section.Children) > 0 {
			calculated.Children = append(calculated.Children, treeSection)
			for j, section2 := range section.Children {
				if len(section2.Children) > 0 {
					calculated.Children[i].Children = append(calculated.Children[i].Children, treeSection)
					for k, section3 := range section2.Children {
						if len(section3.Children) > 0 {
							calculated.Children[i].Children[j].Children = append(calculated.Children[i].Children[j].Children, treeSection)
							for _, section4 := range section3.Children {
								if section4.ID != 0 {
									calculated.Children[i].Children[j].Children[k].Children = append(calculated.Children[i].Children[j].Children[k].Children, treeSection)
								}
							}
						} else if section3.ID != 0 {
							calculated.Children[i].Children[j].Children = append(calculated.Children[i].Children[j].Children, treeSection)
						}
					}
				}
			}
		}
	}
	return
}
