package table

import (
	"eco-passport-21/db"

	"github.com/lib/pq"
	"gitlab.com/gbh007/gojlog"
)

//Values принимает в себя строки данных из базы, которые формируют таблицу на фронте
type Values struct {
	Value []string `json:"value"`
}

//LinesReq запрашивает строки таблиц из базы
func LinesReq(headID, regionID, year int) (value []Values) {
	_db := db.GetDatabase()
	tablereq := `select
					r.value
				 from
					"eco-pasport".eco_pasport."Row" r
				 inner join "eco-pasport".eco_pasport."Table" t on t.id = r.table_id
				 where
					t.header_id = $1
				 	and t.region_id = $2
					and t.year = $3
				 order by
					r.sort`

	var Val Values
	req, err := _db.Query(tablereq, headID, regionID, year)
	if err != nil {
		gojlog.Error(err)
		return
	}
	for req.Next() {
		if err := req.Scan(pq.Array(&Val.Value)); err != nil {
			gojlog.Error(err)
		}
		value = append(value, struct {
			Value []string `json:"value"`
		}{Value: Val.Value})
	}
	return
}
