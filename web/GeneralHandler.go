package web

import (
	"eco-passport-21/general"
	"net/http"

	"gitlab.com/krasecology/go-lib/web"
)

//GetRegion получает информацию о регионах с базы
func GetRegion() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		web.SetResponse(r, general.RegionReq())
	})
}

//GetYear запрос годов из базы
func GetYear() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		web.SetResponse(r, general.YearReq())

	})
}

//GetRegionInfo запрашивает у базы статическую информацию по району. Принимает в себя айди района и год
func GetRegionInfo() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {})
}
