package web

import (
	"eco-passport-21/table"
	"net/http"

	"gitlab.com/krasecology/go-lib/web"
)

//GetTree небольшая прослойка на пути получения дерева, запрашивает все права пользователя у сервера авторизации
func GetTree() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		_, u, _ := web.GetSUPOk(r)
		req := struct {
			Year int `json:"year"`
		}{}
		if web.ParseJSON(r, &req) != nil {
			web.SetError(r, web.ErrParseData)
			return
		}
		web.SetResponse(r, table.TreeSort(u.CalculatedPermissions, req.Year))
	})
}

//GetTable получает ID таблицы и региона
func GetTable() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		req := struct {
			HeaderID int `json:"headerId"`
			RegionID int `json:"regionId"`
			Year     int `json:"year"`
		}{}
		if web.ParseJSON(r, &req) != nil {
			web.SetError(r, web.ErrParseData)
			return
		}
		web.SetResponse(r, table.LinesReq(req.HeaderID, req.RegionID, req.Year))
	})
}

//GetHeader прослойка для получения заголовков таблиц, принимает в себя id таблицы из дерева с фронта
func GetHeader() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		req := struct {
			TableID int `json:"tableId"`
		}{}
		if web.ParseJSON(r, &req) != nil {
			web.SetError(r, web.ErrParseData)
			return
		}
		web.SetResponse(r, table.HeaderReq(req.TableID))
	})
}
