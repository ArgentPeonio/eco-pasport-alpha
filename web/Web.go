package web

import (
	"net/http"
	"time"

	"gitlab.com/gbh007/gojlog"

	//Надо
	_ "gitlab.com/krasecology/go-lib"
	"gitlab.com/krasecology/go-lib/web"
)

// Run запускает веб-сервер
func Run(addr string, staticDir string) <-chan struct{} {
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir(staticDir)))
	web.AddHandler(mux, "/api/table/get-region", "", GetRegion())
	web.AddHandler(mux, "/api/table/get-year", "", GetYear())
	//Принимает в себя год
	web.AddHandler(mux, "/api/table/get-tree", "", GetTree())
	//Принимает в себя id таблицы с фронта
	web.AddHandler(mux, "/api/table/get-header", "", GetHeader())
	//Принимает в себя id таблицы и id региона с фронта
	web.AddHandler(mux, "/api/table/get-table", "", GetTable())

	server := &http.Server{
		Addr:         addr,
		Handler:      mux,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
		IdleTimeout:  1 * time.Minute,
	}
	done := make(chan struct{})
	go func() {
		if err := server.ListenAndServe(); err != nil {
			gojlog.Error(err)
		}
		close(done)
	}()
	return done
}
